
<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Avatar extends Controller {

  public function action_index()
  {
    $view = View::factory('avatar/index');
    $this->response->body($view);
  }

  public function action_delete()
  {
    $id = Arr::get($_POST, 'id', '');
    $img = ORM::factory('Imgnew')
      ->where('id', '=', $id)
      ->find_all();

    foreach($img as $i){
      $asd = $i->src;
      if(unlink("img/$i->src")) $i->delete();
    }

    echo json_encode(array("result"=>false,"msg"=>$img));
  }
  public function action_deletetovar()
  {
    $id = Arr::get($_POST, 'id', '');
    $img = ORM::factory('Tovar')
      ->where('id', '=', $id)
      ->find_all();

    foreach($img as $i){
      if(unlink("img/$i->src")) $i->delete();
    }

    echo json_encode(array("result"=>false,"msg"=>''));
  }
  public function action_deleteservice()
  {
    $id = Arr::get($_POST, 'id', '');
    $img = ORM::factory('Service')
      ->where('id', '=', $id)
      ->find_all();

    foreach($img as $i){
      if(unlink("img/$i->src")) $i->delete();
    }

    echo json_encode(array("result"=>false,"msg"=>''));
  }
  public function action_deletesalon()
  {
    $id = Arr::get($_POST, 'id', '');
    $img = ORM::factory('Imgsalon')
      ->where('id', '=', $id)
      ->find_all();

    foreach($img as $i){
      $asd = $i->src;
      if(unlink("img/$i->src")) $i->delete();
    }

    echo json_encode(array("result"=>false,"msg"=>$img));
  }

  public function action_editService()
  {
    $id = Arr::get($_POST, 'id', '');
    $price = Arr::get($_POST, 'price', '');
    $description = Arr::get($_POST, 'description', '');
    $name = Arr::get($_POST, 'name', '');
    $part = 'tovar';
    $view = View::factory('avatar/upload');
    $error_message = NULL;
    $filename = NULL;
    echo 'asdas';
    echo $id;
    $service = ORM::factory('Service')
      ->where('id', '=', $id)
      ->find_all();
    if (isset($_FILES['avatar']))
    {
      $filename = $this->_save_image($_FILES['avatar'], 1, $part);
    }

    if ( ! $filename)
    {
      $error_message = 'There was a problem while uploading the image.
                Make sure it is uploaded and must be JPG/PNG/GIF file.';
      foreach($service as $i){
        $i->description = $description;
        $i->name = $name;
        $i->price = $price;
        $i->save();
      }
    }else{

      echo $filename . '<br>';
      foreach($service as $i){
        echo $i->src . '<br>';
        if(unlink("img/$i->src")) $i->src = $filename;
        $i->description = $description;
        $i->name = $name;
        $i->price = $price;
        $i->save();
      }
    }

    $view->uploaded_file = $filename;
    $view->error_message = $error_message;
    $this->redirect('/admin');
  }
  public function action_edittovar()
  {
    $id = Arr::get($_POST, 'id', '');
    $desc = Arr::get($_POST, 'desc', '');
    $title = Arr::get($_POST, 'title', '');
    $part = 'tovar';
    $view = View::factory('avatar/upload');
    $error_message = NULL;
    $filename = NULL;
    $tovar = ORM::factory('Tovar')
      ->where('id', '=', $id)
      ->find_all();
    if (isset($_FILES['avatar']))
    {
      $filename = $this->_save_image($_FILES['avatar'], 1, $part);
    }

    if ( ! $filename)
    {
      $error_message = 'There was a problem while uploading the image.
                Make sure it is uploaded and must be JPG/PNG/GIF file.';
      foreach($tovar as $i){
        $i->desc = $desc;
        $i->title = $title;
        $i->save();
      }
    }else{

      foreach($tovar as $i){
        if(unlink("img/$i->src")) $i->src = $filename;
        $i->desc = $desc;
        $i->title = $title;
        $i->save();
      }
    }

    $view->uploaded_file = $filename;
    $view->error_message = $error_message;
    $this->redirect('/admin');
  }
  public function action_editsalon()
  {
    $id = Arr::get($_POST, 'id', '');
    $part = Arr::get($_POST, 'part', '');
    $view = View::factory('avatar/upload');
    $error_message = NULL;
    $filename = NULL;
    $img = ORM::factory('Imgsalon')
      ->where('id', '=', $id)
      ->find_all();
    if (isset($_FILES['avatar']))
    {
      $filename = $this->_save_image($_FILES['avatar'], 1, $part);
    }

    if ( ! $filename)
    {
      $error_message = 'There was a problem while uploading the image.
                Make sure it is uploaded and must be JPG/PNG/GIF file.';
    }else{

      foreach($img as $i){
        $asd = $i->src;
        if(unlink("img/$i->src")) $i->src = $filename;
        $i->save();
      }
    }

    $view->uploaded_file = $filename;
    $view->error_message = $error_message;
    $this->redirect('/admin');
  }
  public function action_editnews()
  {
    $id = Arr::get($_POST, 'id', '');
    $part = Arr::get($_POST, 'part', '');
    $view = View::factory('avatar/upload');
    $error_message = NULL;
    $filename = NULL;
    $img = ORM::factory('Imgnew')
      ->where('id', '=', $id)
      ->find_all();
    if (isset($_FILES['avatar']))
    {
      $filename = $this->_save_image($_FILES['avatar'], 1, $part);
    }

    if ( ! $filename)
    {
      $error_message = 'There was a problem while uploading the image.
                Make sure it is uploaded and must be JPG/PNG/GIF file.';
    }else{

      foreach($img as $i){
        $asd = $i->src;
        if(unlink("img/$i->src")) $i->src = $filename;
        $i->save();
      }
    }

    $view->uploaded_file = $filename;
    $view->error_message = $error_message;
    $this->redirect('/admin');
  }
  public function action_upload()
  {
    $part = Arr::get($_POST, 'part', '');
    $view = View::factory('avatar/upload');
    $error_message = NULL;
    $filename = NULL;
    if ($this->request->method() == Request::POST)
    {
      if (isset($_FILES['avatar']))
      {
        $filename = $this->_save_image($_FILES['avatar'], null , $part);
      }
    }

    if ( ! $filename)
    {
      $error_message = 'There was a problem while uploading the image.
                Make sure it is uploaded and must be JPG/PNG/GIF file.';
    }
    $view->uploaded_file = $filename;
    $view->error_message = $error_message;
    $this->redirect('/admin');
  }
  public function action_uploadService()
  {
    $name = Arr::get($_POST, 'name', '');
    $description = Arr::get($_POST, 'description', '');
    $price = Arr::get($_POST, 'price', '');
    $part = 'tovar';
    $view = View::factory('avatar/upload');
    $error_message = NULL;
    $filename = NULL;
    if ($this->request->method() == Request::POST)
    {
      if (isset($_FILES['avatar']))
      {
        $filename = $this->_save_image($_FILES['avatar'], null , $part);
      }
    }
    $service = new Model_Service();
    $service->src = $filename;
    $service->description = $description;
    $service->price = $price;
    $service->name = $name;
    $service->save();
    if ( ! $filename)
    {
      $error_message = 'There was a problem while uploading the image.
                Make sure it is uploaded and must be JPG/PNG/GIF file.';
    }
    $view->uploaded_file = $filename;
    $view->error_message = $error_message;
    $this->redirect('/admin');
  }
  public function action_uploadTovar()
  {
    $title = Arr::get($_POST, 'title', '');
    $desc = Arr::get($_POST, 'desc', '');
    $part = Arr::get($_POST, 'part', '');
    $view = View::factory('avatar/upload');
    $error_message = NULL;
    $filename = NULL;
    if ($this->request->method() == Request::POST)
    {
      if (isset($_FILES['avatar']))
      {
        $filename = $this->_save_image($_FILES['avatar'], null , $part);
      }
    }
    $service = new Model_Tovar();
    $service->src = $filename;
    $service->desc = $desc;
    $service->title = $title;
    $service->save();
    if ( ! $filename)
    {
      $error_message = 'There was a problem while uploading the image.
                Make sure it is uploaded and must be JPG/PNG/GIF file.';
    }
    $view->uploaded_file = $filename;
    $view->error_message = $error_message;
    $this->redirect('/admin');
  }

  protected function _save_image($image, $edit = null, $part)
  {
    if($part == 'new') $img = new Model_Imgnew();
    if($part == 'salon') $img = new Model_Imgsalon();
    if($part == 'tovar') $img = new Model_Tovar();
    if (
      ! Upload::valid($image) OR
      ! Upload::not_empty($image) OR
      ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
    {
      return FALSE;
    }

    $directory = 'img/';

    if ($file = Upload::save($image, NULL, $directory))
    {
      $filename = strtolower(Text::random('alnum', 20)).'.jpg';
      if ($edit == null)
      {
        if($part != 'tovar'){
          $img->src = $filename;
          $img->save();

        }
      }

      Image::factory($file)
        //->resize(305, 305, Image::AUTO)
        ->save($directory.$filename);

      // Delete the temporary file
      unlink($file);

      return $filename;
    }

    return FALSE;
  }

}
