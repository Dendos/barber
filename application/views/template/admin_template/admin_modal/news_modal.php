<form  action="<?php echo URL::site('avatar/editnews') ?>" method="post" enctype="multipart/form-data">
  <div class="modal fade" id="myModal<?= $i->id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel<?= $i->id?>">Новости <?= $i->id?></h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div class="col-md-12">
              <img class = 'pull-left' style="margin-bottom:20px;" src="img/<?= $i->src;?>" alt="">
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-4">
              <label >Добавить фото маленькое</label>
            </div>
            <div class="col-xs-8">
              <span class="col-xs-2">
                <input type="file" class="form-control" name = 'avatar'>
                <input type="text" class="form-control hidden" name = 'id' value="<?= $i->id?>">
                <input type="text" class="form-control hidden" name = 'part' value="new">

              </span>
              <span class="col-xs-10">
                <p class="desc">
                  размер загрудаемого изображения 305x305px
                </p>
              </span>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-cofirm">сохранить</button>
          <button type="button" class="btn btn-delete delete_img" value="<?= $i->id?>" data-dismiss="modal">удалить</button>
        </div>
      </div>
    </div>
  </div>
</form>
