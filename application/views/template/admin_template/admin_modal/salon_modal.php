<form  action="<?php echo URL::site('avatar/editsalon') ?>" method="post" enctype="multipart/form-data">
  <div class="modal fade" id="salon_modal<?= $img->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Фото салона <?= $img->id;?></h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <img class = 'pull-left' style="margin-bottom:20px;" src="img/<?= $img->src;?>" alt="">
          </div>
          <div class="form-group">
            <div class="col-xs-4">
              <label >Добавить фото большое</label>
            </div>
            <div class="col-xs-8">
              <span class="col-xs-2">
                <input type="file" class="form-control" name = 'avatar'>
                <input type="text" class="form-control hidden" name = 'id' value="<?= $img->id?>">
                <input type="text" class="form-control hidden" name = 'part' value="salon">
              </span>
              <span class="col-xs-10">
                <p class="desc">
                  размер загрудаемого изображения 990x660px
                </p>
              </span>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-cofirm">сохранить</button>
          <button type="button" class="btn btn-delete delete_salon_img" value="<?= $img->id;?>" data-dismiss="modal" >удалить</button>
        </div>
      </div>
    </div>
  </div>
</form>
