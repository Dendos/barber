<form  action="<?php echo URL::site('avatar/edittovar') ?>" method="post" enctype="multipart/form-data">
  <div class="modal fade" id="product_<?= $t->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Редактирование товара <?= $t->title;?></h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <img class = 'pull-left' style="margin-bottom:20px;" src="img/<?= $t->src;?>" alt="">
          </div>
          <div class="form-group">
            <div class="col-xs-4">
              <label >Название товара</label>
            </div>
            <div class="col-xs-8">
              <p class="desc">
                <input name="title" type="text" class="form-control" value="<?= $t->title;?>" required>
              </p>
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-4">
              <label >Добавить фото маленькое</label>
            </div>
            <div class="col-xs-8">
              <span class="col-xs-2">
                <input type="file" class="form-control" name = 'avatar'>
                <input type="text" class="form-control hidden" name = 'id' value="<?= $t->id?>">
                <input type="text" class="form-control hidden" name = 'part' value="tovar">
              </span>
              <span class="col-xs-10">
                <p class="desc">
                  размер загрудаемого изображения 305x305px
                </p>
              </span>
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-4">
              <label >Описание</label>
            </div>
            <div class="col-xs-8">
              <textarea name="desc" value = '<?= $t->desc;?>' required ><?= $t->desc;?></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-cofirm">сохранить</button>
          <button type="button" class="btn btn-delete" data-dismiss="modal">удалить</button>
        </div>
      </div>
    </div>
  </div>
</form>
