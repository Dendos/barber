<div id="servicesModal<?= $s->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" class="modal fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content services-modal"><button type="button" class="close_but" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="images/arrow-bot.png" alt=""></span></button>
      <div class="services-modal-content">
        <h3><?= $s->name; ?></h3>
        <div class="col-md-12 service_img">
          <img class = 'pull-left' style="margin-bottom:20px;" src="img/<?= $s->src;?>" alt="">
        </div>
        <p class="desc">от <?=  $s->price;?> руб.</p>
        <p class="desc" style="margin-bottom:30px;"><?=  $s->description;?></p>
      </div>
    </div>
  </div>
</div>
