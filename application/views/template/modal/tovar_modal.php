<div id="tovar_modal<?= $t->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" class="modal fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content services-modal"><button type="button" class="close_but" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="images/arrow-bot.png" alt=""></span></button>
      <div class="services-modal-content">
        <h3><?= $t->title; ?></h3>
        <div class="col-md-12 service_img">
          <img class = 'pull-left' style="margin-bottom:20px;" src="img/<?= $t->src;?>" alt="">
        </div>
        <p class="desc" style="margin-bottom:30px;"><?=  $t->desc;?></p>
      </div>
    </div>
  </div>
</div>
